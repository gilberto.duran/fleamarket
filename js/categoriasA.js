$(document).ready(function(){
  $.ajax({
    url: "../html/categoriasA.php",
    type: "post",
    data: {getAll:'all'},
    success: function(data){
      made(data);
    }
  });

  $('#btnNew').click(function(){
    window.location.href='newC.html';
  });
});

$(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
          $('#scroll').fadeIn();
      } else {
          $('#scroll').fadeOut();
      }
  });
$('#scroll').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });

function made(data){
  for(let i in data){
    var nodeTabla=document.getElementById("cuerpo");
    var nodeTR=document.createElement('tr');
    var nodeID=document.createElement('td');
    var nodeC=document.createElement('td');

    nodeID.innerHTML=data[i].id;
    nodeC.innerHTML=data[i].nombre;

    nodeTR.appendChild(nodeID);
    nodeTR.appendChild(nodeC);

    nodeTabla.appendChild(nodeTR);
  }
}
