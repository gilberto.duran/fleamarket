var shoes = [{
    name: "Nike Air Max 97 Have A Nike Day Indigo",
    price: "180",
    img: "../img/97_nikeDay.jpg",
    html: "../html/Nike_Air_Max_97.html"
    }, {
    name: "Nike Jordan 1 Retro High Satin Black Toe",
    price: "480",
    img: "../img/jordan1.jpg",
    html: "../html/Air_Jorada_1.html"
    }, {
    name: "Nike Air Max 720 University Red Blue Fury", // Blue Fury
    price: "129",
    img: "../img/720_rbf.jpg",
    html: "../html/Nike_Air_Max_720.html"
    }, {
    name: "adidas Yeezy Boost 350 V2 Clay",
    price: "218",
    img: "../img/yzb350Clay.jpg",
    html: "../html/adidas_YeezyB_V2.html"
    }, {
    name: "Nike React Element 55 White Psychic Purple Hyper Pink", //
    price: "109",
    img: "../img/react_wpphp.jpg",
    html: "../html/Nike_React_Element_55.html"
    }, {
    name: "Kyrie 5 Spongebob Patrick",
    price: "180",
    img: "../img/kyrie_spP.jpg",
    html: "../html/Kyrie_5_Spongebob_P.html"
    }, {
    name: "adidas Yeezy Boost 700 Wave Runner Solid Grey", //
    price: "400",
    img: "../img/yzb700rsg.jpg",
    html: "../html/adidas_Yeezy_700.html"
    }, {
    name: "Nike Air Max 97 Off-White Menta",
    price: "999",
    img: "../img/97_offwhiteMenta.jpg",
    html: "../html/Nike_Air_Max97_OW.html"
    }, {
    name: "Jordan 1 Retro High Off-White Chicago",
    price: "5218",
    img: "../img/jordan1_offWhiteChicago.jpg",
    html: "../html/aj1Chicago_ow.html"
    }, {
    name: "adidas Kamanda Dragon Ball Z Majin Buu",
    price: "250",
    img: "../img/dgbz_Majinbu.jpg",
    html: "../html/adidas_Kamanda_DBZ.html"
    }, {
    name: "Air Force 1 Low VLONE (2017)",
    price: "4450",
    img: "../img/afo_LowVLONE.jpg",
    html: "../html/Nike_AF1_VL.html"
}]
var carrito=[];
localStorage.setItem("inventario", JSON.stringify(shoes));
var btnCarrito = document.getElementById('carrito_btn');

//listener para detectar el leave del mouse
btnCarrito.addEventListener('mouseleave', function(){
    if(sessionStorage['carrito']){
        //Si el carrito existe, se cambia el color del carrito
        btnCarrito.innerHTML="<img src='../img/carrito.png'></img>";
        btnCarrito.style.background="transparent";
        btnCarrito.style.color="#fff";
        //obtengo los datos actuales del carrito y los guardo en una variable auxiliar
        //para evitar perder los datos actulaes despues de guardar un nuevo registro
        var sub = sessionStorage.getItem('carrito');
        var s=JSON.parse(sub);
        for(let i in s)
          carrito[i]=s[i];
      }else{
        btnCarrito.innerHTML="<img src='../img/carrito.png'></img>";
        btnCarrito.style.background="transparent";
        btnCarrito.style.color="#fff";
      }
})

//Verifico que exista el sessionStorage del carrito para evitar errores
if(sessionStorage['carrito']){
  //Si el carrito existe, se cambia el color del carrito
  btnCarrito.innerHTML="<img src='../img/carrito.png'></img>";
  //obtengo los datos actuales del carrito y los guardo en una variable auxiliar
  //para evitar perder los datos actulaes despues de guardar un nuevo registro
  var sub = sessionStorage.getItem('carrito');
  var s=JSON.parse(sub);
  for(let i in s)
    carrito[i]=s[i];
}

window.onload = renderItems;

var botones;

function renderItems() {
    var items = document.getElementById("principal");
    for (let i in shoes) {
        //estructura
        let card = document.createElement('div');
        card.classList.add('card');

        let imagen = document.createElement('img');
        imagen.src = shoes[i].img;

        //estructura interna
        let div_inter = document.createElement('div');

        let titulo = document.createElement('h4');
        titulo.textContent = shoes[i].name;

        let precio = document.createElement('h5');
        precio.textContent = '$' + shoes[i].price;

        let descripcion = document.createElement('p');
        descripcion.textContent = shoes[i].descripcion;

        let boton = document.createElement('a');
        boton.classList.add('button');
        boton.href = shoes[i].html;
        boton.textContent = 'Ver más';
        boton.setAttribute("id",shoes[i].name);
        /*boton.addEventListener('click', function () {
            carrito.push(boton.id);
            console.log(carrito);
            sessionStorage.setItem("carrito", JSON.stringify(carrito));
            btnCarrito.innerHTML="<img src='../img/carrito-compra.png'></img> See more!";
        });*/

        //html construido
        div_inter.appendChild(titulo);
        div_inter.appendChild(precio);
        div_inter.appendChild(descripcion);
        div_inter.appendChild(boton);

        card.appendChild(imagen);
        card.appendChild(div_inter);

        items.appendChild(card);

        /*let carrito_actual = JSON.parse(localStorage.getItem('carro'));
        if (!(carrito_actual == null)) {
            carrito = carrito_actual;
        }*/
    }
}
