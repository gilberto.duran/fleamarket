$(document).ready(function(){
  $("#btnNew").click(function(){
    sessionStorage.setItem('type','admin');
    window.location.href='newUser.php?usertype=admin';
  });
  getAll();
  $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#scroll').fadeIn();
        } else {
            $('#scroll').fadeOut();
        }
    });
    $('#scroll').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    }); 
});

function getAll(){
  $.ajax({
    url: "../html/admin.php",
    type: "post",
    data: {userType:'admin'},
    success: function(data){
      made(data);
    }
  });
}

function made(data){
  for(let i in data){
    var nodeTabla = document.getElementById('cuerpo');
    var nodeTR = document.createElement('tr');
    var nodeNum = document.createElement('td');
    var nodeAvatar = document.createElement('td');
    var nodeName = document.createElement('td');
    var nodeLastName = document.createElement('td');
    var nodeEmail = document.createElement('td');
    var nodeUsername = document.createElement('td');
    var nodeTelefono = document.createElement('td');
    var nodeDireccion = document.createElement('td');
    var nodeModificar = document.createElement('td');
    if(data[i].username!=sessionStorage.getItem('username'))
      var nodeEliminar = document.createElement('td');
    var img = document.createElement('img');

    img.setAttribute('src',"../"+data[i].avatar);
    img.setAttribute('id', 'imgProducto');
    img.className += "zapato";
    nodeAvatar.appendChild(img);
    if(data[i].username!=sessionStorage.getItem('username')){
      nodeEliminar.setAttribute('id', data[i].username);
      nodeEliminar.setAttribute('class', 'borro');
    }
    nodeModificar.setAttribute('id', data[i].username);
    nodeModificar.setAttribute('class', 'mod');
    nodeNum.innerHTML=data[i].id;
    nodeName.innerHTML=data[i].Nombres;
    nodeLastName.innerHTML=data[i].Apellidos;
    nodeEmail.innerHTML=data[i].Email;
    nodeUsername.innerHTML=data[i].username;
    nodeTelefono.innerHTML=data[i].Telefono;
    nodeDireccion.innerHTML=data[i].Direccion;
    if(data[i].username!=sessionStorage.getItem('username'))
      nodeEliminar.innerHTML="<img src='../img/delete.png' class='boton-delete'>";
    nodeModificar.innerHTML="<img src='../img/modificar.png' class='boton-delete'>";

    nodeTR.appendChild(nodeNum);
    nodeTR.appendChild(nodeAvatar);
    nodeTR.appendChild(nodeName);
    nodeTR.appendChild(nodeLastName);
    nodeTR.appendChild(nodeEmail);
    nodeTR.appendChild(nodeUsername);
    nodeTR.appendChild(nodeTelefono);
    nodeTR.appendChild(nodeDireccion);
    nodeTR.appendChild(nodeModificar);
    if(data[i].username!=sessionStorage.getItem('username'))
      nodeTR.appendChild(nodeEliminar);

    nodeTabla.appendChild(nodeTR);

    if(data[i].username!=sessionStorage.getItem('username')){
      $(".borro").click(function(){
        id = $(this).attr("id");
        $.ajax({
          url: "../html/admin.php",
          type: "post",
          data: {delete:id},
          success: function(data){
            if(data){
              window.location.href='admins.html';
            }
          }
        });
      });
    }

    $(".mod").click(function(){
      id = $(this).attr("id");
      sessionStorage.setItem('id',id);
      sessionStorage.setItem('type','admin');
      window.location.href=('modUser.html');
    });
  }
}

function autocompletar(){
  var min = 1;
  var palabra = $('#busqueda').val();
  if(palabra.length>=min){
    $.ajax({
      url: "../html/admin.php",
      type: "post",
      data: {buscar:palabra,usertype:'admin'},
      success: function(data){
        $("#rieles_opc").show();
        for(let i in data){
          var clas='';
          if(i%2==0){
            clas='par';
          }else{
            clas='impar';
          }

          $("#rieles_opc").html("<li onclick=\"set_item('"+data[i].Nombres+" "+data[i].Apellidos+"')\" class='"+clas+"'>"+data[i].Nombres+" "+data[i].Apellidos+"</li>");
        }

      }
    });
  }else{
      $("#rieles_opc").hide();
      var nodeTabla = document.getElementById('cuerpo');
      nodeTabla.innerHTML='';
      getAll();
  }

}

function set_item(opciones){
  $("#busqueda").val(opciones);
  $("#rieles_opc").hide();

  $.ajax({
    url: 'getRiel.php',
    type: 'post',
    data: {riel:opciones},
    success: function(data){
      $("#principal").html(data);
    }
  });
}

function buscar(){
  var buscar=$("#busqueda").val();
  $("#rieles_opc").hide();
  $.ajax({
    url: "../html/admin.php",
    type: "post",
    data: {buscar:buscar,usertype:'admin'},
    success: function(data){
      var nodeTabla = document.getElementById('cuerpo');
      nodeTabla.innerHTML='';
      made(data);
    }
  });
}

function focused(){
  $("#rieles_opc").show();
}
