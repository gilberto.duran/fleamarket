window.onload = init;
var num = 0
var subT = 0;
var carrito = [];


function init() {

  var carrito = [];
  var compra = [];
  var imagenes = [];
  var sub = sessionStorage.getItem('carrito');
  var s = JSON.parse(sub);
  for (let i in s)
    carrito[i] = s[i];

  //var dato = JSON.parse(carrito);
  console.log((carrito));
  $.ajax({
    url: 'carrito_copy.php',
    type: 'get',
    data: carrito,
    success: function(dat) {
        console.log(dat);
    },
    error: function() {
      console.log('error');
    }
  });
}

function findCantidad(carrito, flag) {
  var aux = 0;
  for (var i = 0; i < carrito.length; i++) {
    if (carrito[i].name == flag) {
      aux++;
    }
  }
  return aux;
}

function limpiar() {

}

function saber(e) {
  var tag = e.srcElement.id;

  alert("El elemento selecionado ha sido " + tag);
}

function made(dat) {
  var nodeTabla = document.getElementById('cuerpo');

  var nodeToClean;
  num++;
  //creacion de los elementos
  var nodeTR = document.createElement('tr');
  var nodeNum = document.createElement('td');
  var nodeName = document.createElement('td');
  var nodePrice = document.createElement('td');
  var nodeCantidad = document.createElement('td');
  var nodeTotal = document.createElement('td');
  var nodeBorrar = document.createElement('td');
  var nodeImg = document.createElement('td');
  var img = document.createElement('img');

  img.setAttribute('src', dat.img);
  img.setAttribute('id', 'imgProducto');
  img.className += "zapato";
  nodeImg.appendChild(img);
  nodeBorrar.setAttribute('id', dat.Nombre);
  nodeBorrar.setAttribute('class', 'borro');
  //agragando datos a los elementos
  nodeNum.innerHTML = num;
  nodeName.innerHTML = dat.Nombre;
  nodePrice.innerHTML = "$" + dat.Precio;
  //var valor=findCantidad(compra,compra[i].name);
  nodeCantidad.innerHTML = "1";
  subT += parseFloat(dat.Precio);
  nodeTotal.innerHTML = "$" + dat.Precio;
  nodeBorrar.innerHTML = "<img src='../img/delete.png' class='boton-delete'>"
  //construyendo el arbol del dom
  nodeTR.appendChild(nodeNum);
  nodeTR.appendChild(nodeImg);
  nodeTR.appendChild(nodeName);
  nodeTR.appendChild(nodePrice);
  nodeTR.appendChild(nodeCantidad);
  nodeTR.appendChild(nodeTotal);
  nodeTR.appendChild(nodeBorrar);
  nodeTabla.appendChild(nodeTR);

  var nodeSubT = document.getElementById('subtotal');
  nodeSubT.innerHTML = "$" + subT.toFixed(2);
  var nodeDesc = document.getElementById('desc');
  nodeDesc.innerHTML = "$0.00";
  var nodeEnvio = document.getElementById('entrega');
  var nodeTotal = document.getElementById('total');
  if (!(num >= 2)) {
    subT += 10;
    nodeEnvio.innerHTML = "$10.00";
  } else {
    nodeEnvio.innerHTML = "$0.00";
  }

  nodeTotal.innerHTML = "$" + subT.toFixed(2);
  $(".borro").click(function() {

    id = $(this).attr("id");
    var num = carrito.indexOf(id);
    carrito.splice(num, 1);
    sessionStorage.removeItem("carrito");
    sessionStorage.setItem("carrito", JSON.stringify(carrito));
    console.log(carrito);
    nodeTabla.innerHTML = "";
    init();
  });
}
