$(document).ready(function() {
  var cat = ($('#cat').val());
  buscar(cat);

  $('#cat').change(function() {
    var cat = ($('#cat').val());
    clear();
    buscar(cat);
  });

  $('#modificar').click(function() {
    var img = document.getElementById("imgR").files[0];
    if (document.getElementById("imgR").files.length==0 || $("#nombre").val() == 0 || $("#precio").val() == 0 || $("#desc").val() == 0 || $("#mat").val() == 0) {
      Swal.fire({
        type: 'error',
        title: 'Llena todos los campos',
        text: 'No puedes dejar campos vacios, toda la informacion es importante',
      });
    }else{
      var tallasT = [];
      $("tr").find("td.valoresD").each(function() {
        tallasT.push({
          'tID': $(this).find('input').attr("name"),
          'tVal': $(this).find('input').val()
        });
      });
      let req = new XMLHttpRequest();
      let formData = new FormData();
      formData.append("photo", img);
      req.open("POST", '../img/subir.php');
      req.send(formData);
      var direccionA = $("#imgR").val();
      var subD = direccionA.split("\\");
      var dirFin = "../img/" + subD[2];
      var json = {
        'img': dirFin,
        'nombre': $("#nombre").val(),
        'marca': $("#marca").val(),
        'seller': $("#seller").val(),
        'precio': $("#precio").val(),
        'cat': $("#cat").val(),
        'desc': $("#desc").val(),
        'mat': $("#mat").val(),
        'tallas': tallasT
      };

      console.log(JSON.stringify(json));
      $.ajax({
        url: 'newShoes.php',
        type: 'post',
        data: {
          'array': JSON.stringify(json)
        },
        success: function(dat) {
          Swal.fire({
            type: 'success',
            title: 'Nuevo producto',
            text: 'Grasa nueva para la raza',
          }).then(function() {
            window.history.go(-1);
          });
        },
        error: function(dat) {
          console.log(dat);
        }
      });
    }
  });
});

function made(dat) {
  var nodeTabla = document.getElementById('Ttallas');
  var nodeTR = document.createElement('tr');

  var nodeTDL = document.createElement('td');
  var nodeLabel = document.createElement('label');
  var nodeTDI = document.createElement('td');
  var nodeInput = document.createElement('input');

  nodeInput.setAttribute('type', 'number');
  nodeInput.setAttribute('min', 0);
  nodeInput.setAttribute('name', dat.id);
  nodeInput.setAttribute('id', 'sin');
  nodeInput.setAttribute('required', 'required');
  nodeInput.setAttribute('value', 0);
  nodeTDI.setAttribute('class', 'valoresD');

  nodeLabel.innerHTML = dat.talla;

  nodeTDL.appendChild(nodeLabel);
  nodeTDI.appendChild(nodeInput);
  nodeTR.appendChild(nodeTDL);
  nodeTR.appendChild(nodeTDI);

  nodeTabla.appendChild(nodeTR);
}

function buscar(cat) {
  $.ajax({
    url: 'newShoes.php',
    type: 'post',
    data: {
      'obtain': cat
    },
    success: function(dat) {
      $.each(dat, function(index, value) {
        made(value);
      });
    },
    error: function() {
      console.log('error');
    }
  });
}

function clear() {
  var nodeTabla = document.getElementById('Ttallas');
  nodeTabla.innerHTML = "";
}
