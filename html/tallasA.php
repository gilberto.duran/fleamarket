<?php
include '../php/conexion.php';
  if(isset($_POST['getAll'])){
    $bdd=new conexion();
    $retornable=$bdd->getTallasC();
    header('Content-Type: application/json');
    echo json_encode($retornable, JSON_FORCE_OBJECT);
  }elseif(isset($_POST['new'])){
    $bdd=new conexion();
    $print='<div id="contenedor_registro" class="contenedor-formulario">
      <form action="tallasA.php" class="formulario" name="formulario_registro" id="formulario_registro" method="post" enctype="multipart/form-data">
        <div class="input-group">
          <input type="text" name="talla" id="talla" required value="">
          <label class="label" for="talla">Talla:</label>
        </div>';
      $print.='
          <div class="input-group">
            <label class="labelSelect" id="label3" for="image">Escoge la categoria:</label>
            <select id="cat" name="cat">';
            $retornable=$bdd->getCategoria();
            while($r=mysqli_fetch_array($retornable)){
                $print.="<option value=".$r['id']." selected>".$r['nombre'].'</option>';
            }
            $print.='</select>
          </div>
          <input type="submit" name="newT" id="modificar" value="Registrar">
        </form>
      </div>';

      echo $print;
  }elseif (isset($_POST['newT'])) {
    $bdd=new conexion();
    if($bdd->newTalla($_POST['talla'],$_POST['cat'])){
      echo "<html>
      <head>
          <meta charset='utf-8'>
          <title>Solo Rieles - Datos del usuario</title>
          <meta name='viewport' content='width=device-width, initial-scale=1'>
          <link rel='stylesheet' href='../css/modificar_datos.css'>
          <link rel='stylesheet' href='../pluggins/mensaje.min.css'>
          <link href='https://fonts.googleapis.com/css?family=Open+Sans&display=swap' rel='stylesheet'>
          <script src='../js/jquery-3.4.1.min.js' charset='utf-8'></script>
          <script src='../pluggins/mensaje.min.js'></script>
      </head>
      <body>
      <script type='text/javascript'>
      Swal.fire({
          type: 'success',
          title: 'Datos ingresados con exito',
          text: 'Una nueva talla',
      }).then(function () {
        window.history.go(-2);
      });
      </script>
      </body>
      </html>
      ";
    }
  }else{
    echo "no tienes acceso";
  }
 ?>
