<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <title>Solo Rieles - Registro</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../css/modificar_datos.css">
  <link rel="stylesheet" href="../pluggins/mensaje.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
  <script src='../js/jquery-3.4.1.min.js' charset='utf-8'></script>
  <script src="../pluggins/mensaje.min.js"></script>
  <script src="../js/newShoe.js" charset="utf-8"></script>
</head>
<body>
  <header>
    <h1>Registrar</h1>
  </header>
  <div class="contenedor">
    <div class="wrap">
      <div id="contenedor_registro" class="contenedor-formulario">
        <form action="newShoe.php" class="formulario" name="formulario_registro" id="formulario_registro" method="post" enctype="multipart/form-data">
          <div class="input-group">
            <input type="file" name="riel" required id="imgR">
            <label class="label" for="nombres">Imagen del producto:</label>
          </div>

          <div class="input-group">
            <input type="text" name="nombre" id="nombre" required>
            <label class="label" for="nombre">Nombre del producto:</label>
          </div>

          <div class="input-group">
            <label class="labelSelect" id="label3" for="image">Escoge la marca:</label>
            <select id="marca" name="marca">
              <?php
              include('../php/conexion.php');
              $bdd=new conexion();

              $retorno=$bdd->getMarca();

              while($r=mysqli_fetch_array($retorno)){
                echo "<option value=".$r['id'].">".$r['Nombre'].'</option>';
              }
               ?>
            </select>
          </div>

          <div class="input-group">
            <label class="labelSelect" id="label3" for="image">Escoge al seller:</label>
            <select id="seller" name="seller">
              <?php
              $retorno=$bdd->getSeller();
              if(isset($_GET['username'])){
                while($r=mysqli_fetch_array($retorno)){
                  if($r['username']==$_GET['username'])
                    echo "<option value=".$r['id']." selected>".$r['Nombre'].'</option>';
                }
              }else{
                while($r=mysqli_fetch_array($retorno)){
                  echo "<option value=".$r['id'].">".$r['Nombre'].'</option>';
                }
              }
               ?>
            </select>
          </div>

          <div class="input-group">
            <input type="number" step='0.01' min=0 name="precio" id="precio" required>
            <label class="label" for="precio">Precio:</label>
          </div>

          <div class="input-group">
            <label class="labelSelect" id="label3" for="image">Escoge la categoria:</label>
            <select id="cat" name="cat">
              <?php
              $retorno=$bdd->getCat();
                while($r=mysqli_fetch_array($retorno)){
                  echo "<option value=".$r['id'].">".$r['nombre'].'</option>';
              }
               ?>
            </select>
          </div>
          <div class="input-group" id="tallas">
            <label class="label">Tallas:</label>
            <table>
              <thead>
                <th><label>Talla</label></th>
                <th><label>Cantidad</label></th>
              </thead>
              <tbody id='Ttallas'>

              </tbody>
            </table>
          </div>

          <div class="input-group">
            <textarea id="desc" name="descripcion" rows="8" cols="80" required></textarea>
            <label class="label" for="descripcion">Descripcion:</label>
          </div>

          <div class="input-group">
            <textarea id='mat' name="materiales" rows="8" cols="80"></textarea>
            <label class="label" for="materiales">Materiales:</label>
          </div>
          <button type="button" name="mod" id="modificar">Registrar</button>
        </form>
      </div>
    </div>
  </div>

  <footer>
    <h3>FleaMarket</h3>
  </footer>
</body>

</html>
