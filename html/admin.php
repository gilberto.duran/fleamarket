<?php
  include('../php/conexion.php');
  if(isset($_POST['username'])){
    $bdd = new conexion();
    $retornable=$bdd->buscarUsuario($_POST['username']);
    echo "<h1>Welcome: ".$retornable['Nombres']." ".$retornable['Apellidos']."</h1>";
  }else if(isset($_POST['userType'])){
    header('Content-Type: application/json');
    $bdd = new conexion();
    $retornable=$bdd->userByType($_POST['userType']);

    echo json_encode($retornable, JSON_FORCE_OBJECT);
  }else if(isset($_POST['delete'])){
    $bdd = new conexion();

    echo $bdd->deleteUser($_POST['delete']);
  }else if(isset($_POST['buscar'])){
    header('Content-Type: application/json');
    $bdd=new conexion();
    $retornable=$bdd->userByTypeByName($_POST['buscar'], $_POST['usertype']);

    echo json_encode($retornable, JSON_FORCE_OBJECT);
  }
 ?>
